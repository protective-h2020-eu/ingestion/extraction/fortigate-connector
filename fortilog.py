#!/usr/bin/python
# Module-ish writen by Lucian Paiusescu @ RoEduNet
# Thank god it works, sheesh...

import subprocess
import os
import csv
import StringIO

# General
logfile = "/var/log/forti.log"
debug = 0

# Define Index for accessing values in event array
IDX_DATE = 0
IDX_TIME = 1
IDX_IP_SRC = 16
IDX_IP_DST = 17
IDX_PORT_SRC = 28
IDX_PORT_DST = 29
IDX_PROTO = 24
IDX_SERVICE = 25
IDX_COUNT = 27
IDX_MSG = 36


# Functions
def bash_cmd(cmd):
	output = subprocess.Popen(['/bin/bash', '-c', cmd], stdout=subprocess.PIPE).communicate()[0]
	return output

def check_file(input):
	if os.path.exists(input):
		return True
	else:
		return False

def forti_log(logfile):
	# Define vars
	dir_path = os.path.dirname(os.path.realpath(__file__))
	last_run = bash_cmd("cat "+logfile+" | grep 'type=ips' | tail -1 | awk '{print $1 \" \" $2 \" \" $3}'").strip()
	last_run_path = dir_path + "/last_run.log"

	if debug:
		print("Current dir: " + dir_path)
		print ("Last date: " + last_run + " Current")
		print ("----")
		print (last_run_path)

	if not check_file(last_run_path):
		# First run
		read_last_run = ".*"
		bash_cmd("echo '' > "+last_run_path+" ")
	else:
		read_last_run = bash_cmd("cat "+last_run_path).strip()


	your_list = []
	if last_run != read_last_run:
		if read_last_run == ".*":
			grepv = "nothingspecialheretoFilter";
		else:
			grepv = read_last_run
		
		# sed -e 's/: /:/g'  - is needed to fix a _whitespace_ in time ->time=17: 04:03<
		process_log = bash_cmd("cat "+logfile+" | grep 'type=ips' | sed -n -e '/"+read_last_run+"/,/*/p' | grep -v '"+grepv+"' | sed -e 's/: /:/g' ")
		bash_cmd("echo '"+last_run+"' > "+last_run_path+" ")

		if debug:
			print process_log

		f = StringIO.StringIO(process_log)
		reader = csv.reader(f, delimiter=',')
		for row in reader:
			your_list.append(row)

		if debug:
			num_rows = str(len(your_list))
			print your_list
			print ("Processed records: "+num_rows)
			print "Read last run is "+read_last_run+" as of last execution, and grepv was "+grepv
	else:
		if debug:
			print("Nothing to do, no new records")

	return your_list

def record(index,row):
	return row[index].split('=')[1]

# Check the existance of logfile before we continue
if not check_file(logfile):
	exit("Please make sure you have a readable logfile, current set: "+logfile)

### This must be placed in the main .py file, don't forget to comment below after tshooting... !!
# process forti log
#forti_log = forti_log(logfile)

#print forti_log
#print "Records: "+str(len(forti_log))

#for row in forti_log:
#	print record(IDX_DATE)+" "+record(IDX_TIME)+" Scale: "+record(IDX_COUNT)+" SRC: "+record(IDX_IP_SRC)+"\tDST: "+record(IDX_IP_DST)+"\tP_SRC: "+record(IDX_PORT_SRC)+"\tP_DST: "+record(IDX_PORT_DST)+"\tPROTO: "+record(IDX_PROTO)+"SERVICE: "+record(IDX_SERVICE)+"\tMSG: "+record(IDX_MSG)
	#print "Date: "+record(0)+" Time: "+record(1)
