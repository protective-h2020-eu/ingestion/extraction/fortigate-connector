# Fortigate connector
- - - -
## Table of contents
[Requirements](#requirements)<br>
[Connector description](#connector-description)<br>
[Example of log file](#example-of-log-file)<br>
[Fortigate configuration](#fortigate-configuration)<br>
[Syslog configuration](#syslog-configuration)<br>
[Example of warden_client-forti.cfg configuration](#example-of-warden_client-forti.cfg-configuration)<br>
- - - -
## Requirements
1. Platform: 
    * Python 2.7+
2. Python packages: 
    *warden_client 3.0+
3. Properly configured Syslog Server and Fortigate (all can be simulated by provided data in **fortigate-full.log** and **short-forti.log**)

## Connector description

It uses a mixture of python with bash subprocesses for reading/filtering input

**warden3-forti-sender.py* reads  logfile (e.g.: /var/log/forti.log) parsing all the lines and populating an array/list for further accesing of needed values.

In order for the connector to keep track of where it left off to avoid sending dups, it saves the date/time of the last processed log entry into last_run.log (auto created) for next reading; (continuing from that date/time onwards)

## Example of log file

Syslog Server#  cat /var/log/forti.log:

Jul 20 12:50:57 admin-nat.roedu.net date=2017-07-20,time=12: 50:57,,,,devname=Nat-FortiGate1,device_id=FG3K9B<hidden>,log_id=0419016384,type=ips,subtype=signature,pri=alert,severity=critical,carrier_ep="N/A",profilegroup="N/A",profiletype="N/A",profile="N/A",src=185.165.29.75,dst=81.X.X.41,src_int="VL118-EXT",dst_int="VL118-IN",policyid=1,identidx=0,serial=1664721,status=dropped,proto=17,service=53413/udp,vd="Protective",count=1,src_port=44229,dst_port=53413,attack_id=42781,sensor="all_default",ref="http://www.fortinet.com/ids/VID42781",user="N/A",group="N/A",incident_serialno=522997468,msg="backdoor: Netcore.Netis.Devices.Hardcoded.Password.Security.Bypass"

You can a more detailed log file in **fortigate-full.log**.

## Fortigate configuration

FortiGate Log settings -> CSV format -> Send to syslog server

## Syslog configuration

Log Rotate for the forti.log, once a month should be acceptable, the lesser rotates the better.

Syslog Server#  cat /etc/logrotate.d/forti 

/var/log/forti.log
{
       rotate 6
        monthly
        missingok
        notifempty
        delaycompress
        compress
        postrotate
                invoke-rc.d rsyslog rotate > /dev/null
        endscript
}

You need to add a crontab entry for frequent 5 min runs.

Syslog Server#  cat /etc/crontab | grep forti

*/5 *   * * *   root    /usr/bin/python /opt/warden3-forti/warden3-forti-sender.py &>> /var/log/warden3-forti-sender.log


## Example of warden_client-forti.cfg configuration
File is required to properly configure information sent to warden
```
{
    "warden": "warden_client.cfg",
    "name": "net.roedu.protective.fortigate1",
    "secret": "",

    "anonymised": "no",
    "target_net": "81.180.251.32/27",

    "a_win": 5
}
```
