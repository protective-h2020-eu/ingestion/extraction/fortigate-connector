#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2011-2015 Cesnet z.s.p.o
# Use of this source is governed by a 3-clause BSD-style license, see LICENSE file.

from warden_client import Client, Error, read_cfg, format_timestamp
import json
import string
from time import time, gmtime, strftime, mktime
import datetime
from math import trunc
from uuid import uuid4
from os import path
import sys
import fortilog as FL

DEFAULT_ACONFIG = 'warden_client-forti.cfg'
DEFAULT_WCONFIG = 'warden_client.cfg'
DEFAULT_NAME = 'org.example.warden.test'
DEFAULT_AWIN = 5
DEFAULT_ANONYMISED = 'no'
DEFAULT_TARGET_NET = '0.0.0.0/0'
DEFAULT_SECRET = ''

logfile = "/var/log/forti.log"

IDX_DATE = 0
IDX_TIME = 1
IDX_IP_SRC = 16
IDX_IP_DST = 17
IDX_PORT_SRC = 28
IDX_PORT_DST = 29
IDX_PROTO = 24
IDX_SERVICE = 25
IDX_COUNT = 27
IDX_MSG = 36

def gen_event_idea(client_name, detect_time, win_start_time, win_end_time, conn_count, src_ip, dst_ip, aggr_win, anonymised, target_net, note, proto, src_port, dst_port):

  event = {
     "Format": "IDEA0",
     "ID": str(uuid4()),
     "DetectTime": detect_time,
     "WinStartTime": win_start_time,
     "WinEndTime": win_end_time,
     "Category": ["Test"],
     "Note": note,
     "ConnCount": conn_count,
     "Source": [{}],
     "Target": [{}],
     "Node": [
        {
           "Name": client_name,
           "Tags": ["Connection","IDS","Attempt.Exploit"],
           "SW": ["FortiGate"],
           "AggrWin": strftime("%H:%M:%S", gmtime(aggr_win))
        }
     ]
  }

  # proto is numeric, need to translate
  protos = {
	'1': 'icmp',
	'6': 'tcp',
	'17': 'udp'
  }

  proto = [protos[proto]]

  af = "IP4" if not ':' in src_ip else "IP6"
  event['Source'][0][af] = [src_ip]
  event['Source'][0]['Port']  = [src_port]
  event['Target'][0]['Port']  = [dst_port]
  event['Target'][0]['Proto'] = proto

  if anonymised != 'omit':
    if anonymised == 'yes':
      event['Target'][0]['Anonymised'] = True
      event['Target'][0][af] = [target_net]
    else:
      event['Target'][0][af] = [dst_ip]
  
  return event

def main():
  aconfig = read_cfg(DEFAULT_ACONFIG)
  wconfig = read_cfg(aconfig.get('warden', DEFAULT_WCONFIG))
  
  aname = aconfig.get('name', DEFAULT_NAME)
  awin = aconfig.get('awin', DEFAULT_AWIN) * 60
  wconfig['name'] = aname

  asecret = aconfig.get('secret', DEFAULT_SECRET)
  if asecret:
    wconfig['secret'] = asecret

  wclient = Client(**wconfig)   

  aanonymised = aconfig.get('anonymised', DEFAULT_ANONYMISED)
  if aanonymised not in ['no', 'yes', 'omit']:
    wclient.logger.error("Configuration error: anonymised: '%s' - possible typo? use 'no', 'yes' or 'omit'" % aanonymised)
    sys.exit(2)

  atargetnet  = aconfig.get('target_net', DEFAULT_TARGET_NET)
  aanonymised = aanonymised if (atargetnet != DEFAULT_TARGET_NET) or (aanonymised == 'omit') else DEFAULT_ANONYMISED

  forti_log = FL.forti_log(logfile)

  events = []

  for row in forti_log:
    date_time = FL.record(IDX_DATE,row)+" "+FL.record(IDX_TIME,row)
    date_time = int(mktime(datetime.datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S").timetuple()))
    dtime = format_timestamp(date_time)
    etime = format_timestamp(time())
    stime = format_timestamp(time() - awin)
    events.append(gen_event_idea(client_name = aname, detect_time = dtime, win_start_time = stime, win_end_time = etime, conn_count = int(FL.record(IDX_COUNT,row)), src_ip = FL.record(IDX_IP_SRC,row), dst_ip = FL.record(IDX_IP_DST,row), aggr_win = awin, anonymised = aanonymised, target_net = atargetnet, note = FL.record(IDX_MSG,row), proto = FL.record(IDX_PROTO,row), src_port = int(FL.record(IDX_PORT_SRC,row)), dst_port = int(FL.record(IDX_PORT_DST,row))))
 
  print "=== Sending ==="
  start = time()
  ret = wclient.sendEvents(events)
  
  if ret:
    wclient.logger.info("%d event(s) successfully delivered." % len(forti_log))

  print "Time: %f" % (time() - start)

if __name__ == "__main__":
    main()
